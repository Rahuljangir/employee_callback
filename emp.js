
//  * Use asynchronous callbacks ONLY wherever possible.
//  * Error first callbacks must be used always.
//  * Each question's output has to be stored in a json file.
//  * Each output file has to be separate.

//  * Ensure that error handling is well tested.
//  * After one question is solved, only then must the next one be executed. 
//  * If there is an error at any point, the subsequent solutions must not get executed.

//  * Store the given data into data.json
//  * Read the data from data.json
//  * Perfom the following operations.


// {
//     "employees": [
//         {
//             "id": 23,
//             "name": "Daphny",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 73,
//             "name": "Buttercup",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 93,
//             "name": "Blossom",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 13,
//             "name": "Fred",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 89,
//             "name": "Welma",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 92,
//             "name": "Charles Xavier",
//             "company": "X-Men"
//         },
//         {
//             "id": 94,
//             "name": "Bubbles",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 2,
//             "name": "Xyclops",
//             "company": "X-Men"
//         }
//     ]
// }

let fs = require('fs');


function readData() {
    return new Promise((resolve, reject) => {
        fs.readFile('data.json', 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                
                resolve(data);
            }
        })
    })
}

//     1. Retrieve data for ids : [2, 13, 23].

function dataForIds(data){
    ids = [2, 13, 23];
    let dataInObj = JSON.parse(data) 
    let dataInArr = dataInObj.employees
    let idsData = dataInArr.filter((curr) => {
        return ids.indexOf(curr.id) !== -1;
    })
    return new Promise((resolve, reject) => {
        fs.writeFile("idsData.json", JSON.stringify(idsData), (err) => {
            if(err){
                reject(err);
            }else{
                resolve(dataInArr);
            }
        })
    })
}




//     2. Group data based on companies.
//         { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}


//     3. Get all data for company Powerpuff Brigade
function PowerpuffData(data){
   
    let dataOfCompany = data.filter((curr) => {
        return curr.company == "Powerpuff Brigade";
    })
    return new Promise((resolve, reject) => {
        fs.writeFile("powerpuff.json", JSON.stringify(dataOfCompany), (err) => {
            if(err){
                reject(err);
            }else{
                resolve(data);
            }
        })
    })
}

//     4. Remove entry with id 2.

function removeId2(data) {
    let afterRemoveId2 = data.filter((curr) => {
        return curr.id != 2;
    })
    return new Promise((resolve, reject) => {
        fs.writeFile("afterRemoveId2.json", JSON.stringify(afterRemoveId2), (err) => {
            if(err){
                reject(err);
            }else{
                resolve();
            }
        })
    })
}
//     5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
//     6. Swap position of companies with id 93 and id 92.
//     7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

readData()
.then((data) => {
    console.log("data.json read successfully : ")
    return dataForIds(data);
})
.then((data) => {
    console.log("file written successfully : ");
    return PowerpuffData(data);
})
.then((data) => {
    console.log("data written successfully : ");
    return removeId2(data)
})
.then(() => {
    console.log("data written successfully : ");
})
.catch((err) => {
    console.log(err);
})